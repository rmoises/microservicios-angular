package com.formacionbdi.microservicios.app.examenes.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionbdi.microservicios.app.examenes.models.repository.AsignaturaRepository;
import com.formacionbdi.microservicios.app.examenes.models.repository.ExamenRespository;
import com.formacionbdi.microservicios.commons.examenes.models.entity.Asignatura;
import com.formacionbdi.microservicios.commons.examenes.models.entity.Examen;
import com.formacionbdi.microservicios.commons.services.CommonServiceImpl;

@Service
public class ExamenServiceImpl extends CommonServiceImpl<Examen, ExamenRespository> implements ExamenService {

	@Autowired
	private AsignaturaRepository asignaturaRespository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Examen> findByNombre(String nombre) {
		return repository.findByNombre(nombre);
	}
	
	@Override
	public Iterable<Asignatura> findAllAsignaturas() {
		return asignaturaRespository.findAll();
	}

	
	
}
