package com.formacionbdi.microservicios.app.cursos.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.formacionbdi.microservicios.app.cursos.models.entity.Curso;
import com.formacionbdi.microservicios.app.cursos.models.entity.CursoAlumno;
import com.formacionbdi.microservicios.app.cursos.services.CursoService;
import com.formacionbdi.microservicios.commons.alumnos.models.entity.Alumno;
import com.formacionbdi.microservicios.commons.controllers.CommonController;
import com.formacionbdi.microservicios.commons.examenes.models.entity.Examen;

@RestController
public class CursoController extends CommonController<Curso, CursoService> {

	@Value("${config.balanceador.test}")
	private String balanceadorTest;
	
	@DeleteMapping("/elimiar-alumno/{id}")
	public ResponseEntity<?> elimiarCursoAlumnoById(@RequestParam Long id) {
		service.elimiarCursoAlumnoById(id);
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/test_balanceador")
	public ResponseEntity<?> balanceadorTest(){
		Map<String, Object> response = new HashMap<>();
		response.put("balanceador", balanceadorTest);
		// response.put("cursos", service.findAll());
		return ResponseEntity.ok(response);
	}
	
	@GetMapping
	@Override
	public ResponseEntity<?> listar(){
		
		List<Curso> cursos = ((List<Curso>) service.findAll()).stream().map(cur -> {
			cur.getCursoAlumnos().forEach(curAl ->{
				Alumno alum = new Alumno();
				alum.setId(curAl.getAlumnoId());
				cur.addAlumno(alum);
			});
			return cur;
		}).collect(Collectors.toList());
		
		return ResponseEntity.ok().body(cursos);
	}
	
	@GetMapping("/pagina")
	@Override
	public ResponseEntity<?> listar(Pageable pageable){
		
		Page<Curso> cursos = service.findAll(pageable).map(curs ->{
			curs.getCursoAlumnos().forEach(curAl ->{
				Alumno alum = new Alumno();
				alum.setId(curAl.getAlumnoId());
				curs.addAlumno(alum);
			});
			return curs;
		});
		
		return ResponseEntity.ok().body(cursos);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id){
		
		Optional<Curso> opEntity = service.findById(id);
		
		if(opEntity.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		Curso oCurso = opEntity.get();
		
		if(oCurso.getCursoAlumnos().isEmpty() == false) {
			List<Long> ids = oCurso.getCursoAlumnos().stream().map(cur -> {
				return cur.getAlumnoId();
			}).collect(Collectors.toList());
			
			List<Alumno> alumnos = (List<Alumno>)service.obtenerAlumnosPorCurso(ids);
			
			oCurso.setAlumnos(alumnos);
		}
		
		return ResponseEntity.ok().body(oCurso);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> editar(@Valid @RequestBody Curso curso, BindingResult result, @PathVariable Long id){
		
		if(result.hasErrors()) {
			return this.validar(result);
		}
		
		Optional<Curso> o = this.service.findById(id);
		
		if(o.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		Curso dbCurso = o.get();
		dbCurso.setNombre(curso.getNombre());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(dbCurso));
	}
	
	@PutMapping("/{id}/asignar-alumnos")
	public ResponseEntity<?> asignarAlumnos(@RequestBody List<Alumno> alumnos, @PathVariable Long id){
		Optional<Curso> o = this.service.findById(id);
		
		if(o.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		Curso dbCurso = o.get();
		
		alumnos.forEach( alu -> {
			CursoAlumno cursoAlumno = new CursoAlumno();
			cursoAlumno.setAlumnoId(alu.getId());
			cursoAlumno.setCurso(dbCurso);
			dbCurso.addCursoAlumnos(cursoAlumno);
		});
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(dbCurso));
	}
	
	@PutMapping("/{id}/eliminar-alumno")
	public ResponseEntity<?> eliminarAlumno(@RequestBody Alumno alumno, @PathVariable Long id){
		Optional<Curso> o = this.service.findById(id);
		
		if(o.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		Curso dbCurso = o.get();
		CursoAlumno curAlumno = new CursoAlumno();
		curAlumno.setAlumnoId(alumno.getId());
		dbCurso.removeCursoAlumnos(curAlumno);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(dbCurso));
	}
	
	@GetMapping("/alumno/{id}")
	public ResponseEntity<?> buscarByAlumnoId(@PathVariable Long id){
		Curso cursoDb = service.findCursoByAlumnoId(id);
		
		if(cursoDb != null) {
			List<Long> examenesIds = (List<Long>) service.obtenerExamenesIdsConRespuestaAlumno(id);
			List<Examen> examenes = cursoDb.getExamenes().stream().map(examen -> {
				if(examenesIds.contains(examen.getId())) {
					examen.setRespondido(true);
				}
				return examen;
			}).collect(Collectors.toList());
			
			cursoDb.setExamenes(examenes);
		}
		
		return ResponseEntity.ok(cursoDb);
	}
	
	@PutMapping("/{id}/asignar-examenes")
	public ResponseEntity<?> asignarExamenes(@RequestBody List<Examen> examenes, @PathVariable Long id){
		Optional<Curso> o = this.service.findById(id);
		
		if(o.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		Curso dbCurso = o.get();
		
		examenes.forEach( exa -> {
			dbCurso.addExamen(exa);
		});
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(dbCurso));
	}
	
	@PutMapping("/{id}/eliminar-examen")
	public ResponseEntity<?> eliminarExamen(Examen examen, @PathVariable Long id){
		Optional<Curso> o = this.service.findById(id);
		
		if(o.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		Curso dbCurso = o.get();
		
		dbCurso.removeExamen(examen);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(dbCurso));
	}
	
}
