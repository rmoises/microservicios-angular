package com.formacionbdi.microservicios.app.respuestas.service;

import com.formacionbdi.microservicios.app.respuestas.models.entity.Respuesta;

public interface RespuestaService {

	public Iterable<Respuesta> saveAll(Iterable<Respuesta> respuestas);
	public Iterable<Respuesta> findRespuestaByAlumnoByExamen(Long idAlumno, Long idExamen);
	public Iterable<Long> findExamanIdConRespuestasByAlumno(Long alumnoId);
}
