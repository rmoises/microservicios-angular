package com.formacionbdi.microservicios.app.respuestas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionbdi.microservicios.app.respuestas.models.entity.Respuesta;
import com.formacionbdi.microservicios.app.respuestas.models.repository.RespuestaRespository;

@Service
public class RespuestaServiceImpl implements RespuestaService {

	@Autowired
	private RespuestaRespository repository;
	
	@Override
	@Transactional
	public Iterable<Respuesta> saveAll(Iterable<Respuesta> respuestas) {
		return repository.saveAll(respuestas);
	}

	@Override
	public Iterable<Respuesta> findRespuestaByAlumnoByExamen(Long idAlumno, Long idExamen) {
		return repository.findRespuestaByAlumnoByExamen(idAlumno, idExamen);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Long> findExamanIdConRespuestasByAlumno(Long alumnoId) {
		return repository.findExamanIdConRespuestasByAlumno(alumnoId);
	}

}
